/* eslint-disable import/no-extraneous-dependencies */
const expect = require('chai').expect;
const gCounterFactory = require('./g-counter.logic');

const gCounterPlusOne = gCounterFactory();

describe('g-counter', () => {
  it('should throw on incorrect size', () => {
    expect(() => gCounterPlusOne({ size: 'abc', id: 0 })).to.throw();
    expect(() => gCounterPlusOne({ size: 2.2, id: 0 })).to.throw();
    expect(() => gCounterPlusOne({ size: null, id: 0 })).to.throw();
    expect(() => gCounterPlusOne({ id: 0 })).to.throw();
  });

  it('should throw on incorrect id', () => {
    expect(() => gCounterPlusOne({ size: 3, id: 4 })).to.throw();
    expect(() => gCounterPlusOne({ size: 5, id: 5 })).to.throw();
    expect(() => gCounterPlusOne({ size: 5, id: 'abc' })).to.throw();
  });

  it('should initialize counter with zeros and initial value zero', () => {
    const gCounter = gCounterPlusOne({ size: 3, id: 0 });
    expect(gCounter.state).to.deep.equal([0, 0, 0]);
    expect(gCounter.value).to.equal(0);
  });

  it('should not allow to modify the state from outside', () => {
    const gCounter = gCounterPlusOne({ size: 3, id: 0 });
    gCounter.state[0] = 5;
    gCounter.state[1] = 1;
    expect(gCounter.state).to.deep.equal([0, 0, 0]);
    expect(gCounter.value).to.equal(0);
  });

  it('supports .increment', () => {
    const gCounter = gCounterPlusOne({ size: 5, id: 4 });
    gCounter.increment();
    gCounter.increment();
    expect(gCounter.value).to.equal(2);
    expect(gCounter.state).to.deep.equal([0, 0, 0, 0, 2]);
  });

  it('throws when merge invariants not met', () => {
    const gCounter = gCounterPlusOne({ size: 4, id: 0 });
    gCounter.increment();
    gCounter.increment();
    expect(() => gCounter.merge()).to.throw();
    expect(() => gCounter.merge('abc')).to.throw();
    expect(() => gCounter.merge([0, 0, 0, 0, 0])).to.throw();
    expect(() => gCounter.merge([3, 0, 0, 0])).to.throw();
  });

  it('support .merge', () => {
    const gCounter = gCounterPlusOne({ size: 4, id: 3 });
    gCounter.increment();
    gCounter.increment();
    gCounter.merge([1, 2, 3, 1]);
    expect(gCounter.state).to.deep.equal([1, 2, 3, 2]);
    expect(gCounter.value).to.equal(8);
  });
});
