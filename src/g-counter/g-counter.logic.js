const DEFAULT_CONFIG = Object.freeze({
  ZERO: 0,
  INC_STEP: 1,

  op: (a, b) => a + b,
  comp: (a, b) => a > b,
  max: (a, b) => Math.max(a, b),
});

const checkPreConditions = ({ state, size, id }) => {
  if (!(Number(size) === size && size % 1 === 0)) {
    throw new Error(`g-counter/incorrect-size: expected a number got: ${size}`);
  }
  if (state[id] !== 0) {
    throw new Error(`g-counter/incorrect-id: expected: [0-${size - 1}] got: ${id}`);
  }
};

const checkMergeInvariant = ({ config, id, payload, state }) => {
  if (!Array.isArray(payload)) {
    throw new Error('g-counter/update-invariant: payload not array');
  }
  if (state.length !== payload.length) {
    throw new Error('g-counter/update-invariant: size mismatch');
  }
  if (config.comp(payload[id], state[id])) {
    throw new Error('g-counter/update-invariant: counter state altered externally.');
  }
};

module.exports = (config = DEFAULT_CONFIG) => ({ size, id }) => {
  let state = new Array(size).fill(0);

  checkPreConditions({ state, size, id });

  return {
    get id() { return id; },
    get state() { return state.slice(0); },
    get value() { return state.reduce(config.op, config.ZERO); }, // todo: (?) memoize

    increment: () => { state[id] = config.op(state[id], config.INC_STEP); },
    merge: (payload) => {
      checkMergeInvariant({ config, id, payload, state });
      state = state.map((v, idx) => config.max(v, payload[idx]));
    },
  };
};
