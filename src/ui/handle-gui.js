/* globals window, document */
const Rx = require('rxjs');
const stylesFn = require('./styles');

// --- FIXME ---

// fixme does not belong here
const reduceToObjectByKey = (arr, keyName) => arr.reduce((acc, c) => {
  acc[c[keyName]] = c; // eslint-disable-line
  return acc;
}, {});

const linearProgress = (start, end) => progress => start + ((end - start) * progress);

// --- ANIMATION ---

const SMOOTH_VALUE_UPDATE_MS = 200;
const ACTIVE_VALUE_MS = 200;

// ideally duration would be timeInMs or (start, end) => timeInMs
const linearAnimation = duration => (valuesStream) => {
  let current; // rembember last value to allow smooth continuation

  const rest = valuesStream
    .skip(1)
    .map(nextValue => ({
      nextValue,
      startValue: current,
      startTime: Date.now(),
    }))
    .switchMap(({ nextValue, startValue, startTime }) =>
      Rx.Observable.if(
        () => (nextValue - startValue <= 1),
        Rx.Observable.of(nextValue),
        Rx.Observable.defer(() => Rx.Observable.of(Date.now()))
          .observeOn(Rx.Scheduler.animationFrame)
          .map(now => (now - startTime) / duration)
          .map(linearProgress(startValue, nextValue))
          .map(v => Math.min(Math.ceil(v), nextValue))
          .repeatWhen(changes => changes.mergeMapTo(
            Rx.Observable.if(
              () => current < nextValue,
              Rx.Observable.of(true),
              Rx.Observable.empty())
            )
          )
          .distinctUntilChanged()
      )
    );

  return valuesStream.take(1)
    .merge(rest)
    .do(value => (current = value));
};

// --- LAYOUT ---

const DEFAULT_APP_SIZE = Rx.Observable.fromEvent(window, 'resize')
  .startWith(1)
  .map(() => ({
    width: window.innerWidth,
    height: window.innerHeight,
  }));

const SIZES = {
  VIEW_BOX: 1024,
  RADIUS: 356,

  COUNTER: 192,
  SATELLITE: 72,
  CROWN: 16,
  MESSAGE: 8,
  MESSAGE_HALF: 4,
};

const calculateXY = ({ cos, sin, totalSize }) =>
  (radius, size, offset = (totalSize + size) / 2) => ({
    x: totalSize - ((Math.round(cos * radius)) + offset),
    y: totalSize - (Math.round(sin * radius) + offset),
  });

const calculatePositions = size =>
  Rx.Observable.range(0, size)
    .map(n => ((2 * Math.PI * n) / size) + (Math.PI / 2))
    .map(arc => calculateXY({
      cos: Math.cos(arc),
      sin: Math.sin(arc),
      totalSize: SIZES.VIEW_BOX,
    }))
    .map(xy => ({
      counter: xy(SIZES.RADIUS, SIZES.COUNTER),
      id: xy(SIZES.RADIUS - (SIZES.COUNTER / 2), SIZES.SATELLITE),
      button: xy(SIZES.RADIUS + (SIZES.COUNTER / 2), SIZES.SATELLITE),
      crown: xy(SIZES.RADIUS - ((SIZES.COUNTER + SIZES.SATELLITE) / 2), SIZES.CROWN),
      crownCenter: xy(SIZES.RADIUS - ((SIZES.COUNTER + SIZES.SATELLITE) / 2), 0),
    }));

const calculateViewBoxPosition = ({
  width,
  height,
  minDim = Math.min(width, height),
  scale = minDim / SIZES.VIEW_BOX,
}) => ({
  scale,
  x: ((width - SIZES.VIEW_BOX) / 2),
  y: ((height - SIZES.VIEW_BOX) / 2),
});

const observeSize = ({ size, root, viewBox }) => size
  .do(({ width, height }) => Object.assign(root.style, { width: `${width}px`, height: `${height}px` }))
  .map(calculateViewBoxPosition)
  .do(({ x, y, scale }) =>
    Object.assign(viewBox.style, { transform: `translate(${x}px, ${y}px) scale(${scale})` }));

// --- ELEMENTS ---

const append = ({ parent, id, style, classNames = [], elType = 'div' }) => {
  const el = document.createElement(elType);
  if (id) { el.id = id; }
  if (style) { Object.assign(el.style, style); }
  classNames.forEach(c => (el.classList += c));
  parent.appendChild(el);
  return el;
};

const mountCounter = ({ counter, viewBox, styles }) =>
  ({ counter: counterPosition, id, button, crown }) => // position info
    [
      [counterPosition, 'counter'],
      [id, 'id', `${counter.id}<div style="opacity:0.5;transform:scale(0.67)">id</div>`],
      [button, 'button', '+', 'button'],
      [crown, 'crown', ''],
    ]
      .reduce((acc, [{ x, y }, name, value, elType]) => {
        const shell = append({
          elType,
          parent: viewBox,
          id: `${name}-${counter.id}`,
          style: Object.assign({ transform: `translate(${x}px, ${y}px)` }, styles[name]),
        });
        const content = append({
          parent: shell,
          id: `${name}Content-${counter.id}`,
          style: Object.assign({}, styles.defaultContent, styles[`${name}Content`]),
        });
        acc[name] = {  // eslint-disable-line
          shell,
          content,
          setContent: v => (content.innerHTML = v),
          updateStyle: style => Object.assign(shell.style, style),
        };
        acc[name].setContent(value);
        return acc;
      }, {});

const mountCounters = ({ viewBox, counters, styles }) =>
  calculatePositions(counters.length)
    .toArray()
    .mergeMap(allPositions => // keeps original order
      Rx.Observable.from(counters)
        .map((counter, idx) => ({
          counterId: counter.id,
          element: mountCounter({ counter, viewBox, styles })(allPositions[idx]),
          positions: allPositions[idx],
        }))
    )
    .toArray();

const displayMessagesBatch = ({ viewBox, styles }) => positionsAndScale =>
  Rx.Observable.create(() => {
    const elements = positionsAndScale
      .map(p => ({
        ...p,
        size: p.scale * SIZES.MESSAGE,
        sizeHalf: (p.scale * SIZES.MESSAGE) / 2,
        scale: p.scale,
      }))
      .map(p => append({
        parent: viewBox,
        style: {
          ...styles.message,
          width: `${p.size}px`,
          height: `${p.size}px`,
          transform: `translate(${p.x - p.sizeHalf}px, ${p.y - p.sizeHalf}px)`,
        },
        classNames: ['disappearing'],
      }));
    elements.forEach(e => { e.style.opacity = 0; }); // eslint-disable-line
    return () => elements.forEach(e => viewBox.removeChild(e));
  });

// --- VALUES & INCREMENTS

const clicksToId = Rx.Observable.fromEvent(document, 'click', true)
  .filter(e => (e.target.id || '').indexOf('button') === 0)
  .map(e => parseInt(e.target.id.split('-')[1], 10));

const keyboardToId = Rx.Observable.fromEvent(document, 'keypress')
  .map(e => e.key)
  .map(n => parseInt(n, 10));

const handleIncrements = countersById => clicksToId.merge(keyboardToId)
  .map(id => countersById[id])
  .filter(Boolean)
  .do(c => c.increment());

const connectCountersToValues = ({ counters, elements, styles }) =>
  Rx.Observable.merge(...counters
    .map((c, idx) => ([
      c.values
        .startWith(c.startValue)
        .distinctUntilChanged()
        .let(linearAnimation(SMOOTH_VALUE_UPDATE_MS)),
      elements[idx].counter.setContent,
      elements[idx].counter.updateStyle,
    ]))
    .map(([values, setContent, updateStyle]) =>
      values
        .do(setContent)
        .switchMap(
          () => Rx.Observable.merge(
            Rx.Observable.of(true)
              .do(() => updateStyle({ borderColor: styles.COLORS.ACCENT })),
            Rx.Observable.timer(ACTIVE_VALUE_MS)
              .do(() => updateStyle({ borderColor: styles.COLORS.DARK_PRIMARY }))
          )
        )
    )
  );

// --- UPDATES ---

const visualizeUpdate = (viewBox, styles) => ({ deliveryTime, source, to }) =>
  Rx.Observable.of(to.map(t => ({
    x: linearProgress(source.center.x, t.center.x),
    y: linearProgress(source.center.y, t.center.y),
  })))
    .do(() => source.element.updateStyle({ backgroundColor: styles.COLORS.ACCENT }))
    .mergeMap(messagesXY =>
      Rx.Observable.from([0, 100])
        .let(linearAnimation(deliveryTime))
        .map(progress => progress / 100)
        .map(progress => messagesXY.map(m => ({
          x: Math.round(m.x(progress)),
          y: Math.round(m.y(progress)),
          scale: 2 * (0.8 - Math.abs(0.5 - progress)),
        })))
        .mergeMap(displayMessagesBatch({ viewBox, styles }))
        .takeUntil(
          Rx.Observable.timer(deliveryTime)
            .do(() => source.element.updateStyle({ backgroundColor: styles.COLORS.LIGHT_PRIMARY })))
    );

const visualizeUpdates = ({ viewBox, styles, updates, crownsById }) => updates
  .mergeMap(update => Rx.Observable.of(update))
  .map(({ sourceId, deliveryTime, to }) => ({
    deliveryTime,
    source: crownsById[sourceId],
    to: to.map(id => crownsById[id]),
  }))
  .mergeMap(visualizeUpdate(viewBox, styles));

// --- RETURN ---

module.exports = rootId => ({ counters, updates, size = DEFAULT_APP_SIZE }) => {
  const styles = stylesFn(SIZES); // todo add themes
  const root = document.getElementById(rootId);
  Object.assign(root.style, styles.root);
  const viewBox = append({ parent: root, style: styles.viewBox });
  const countersById = reduceToObjectByKey(counters, 'id');

  return Rx.Observable.merge(
    observeSize({ size, root, viewBox }),
    mountCounters({ viewBox, counters, styles })
      .map(elementsAndPositions => ({
        elements: elementsAndPositions.map(x => x.element),
        crownsById: reduceToObjectByKey(elementsAndPositions
          .map(x => ({
            id: x.counterId,
            element: x.element.crown,
            center: x.positions.crownCenter,
          })), 'id'),
      }))
      .mergeMap(({ elements, crownsById }) =>
        Rx.Observable.merge(
          connectCountersToValues({ counters, elements, styles }),
          visualizeUpdates({ viewBox, styles, updates, crownsById })
        )
      ),
    handleIncrements(countersById),
  );
};
