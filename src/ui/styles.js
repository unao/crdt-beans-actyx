const COLORS = {
  DARK_PRIMARY: '#303F9F',
  DEFAULT_PRIMARY: '#3F51B5',
  LIGHT_PRIMARY: '#C5CAE9',
  TEXT_PRIMARY: '#FFFFFF',
  ACCENT: '#8BC34A',
  PRIMARY_TEXT: 'rgba(0, 0, 0, 0.67)',
  SECONDARY_TEXT: '#757575',
  DIVIDER_COLOR: '#BDBDBD',
};

const satellite = SIZES => ({
  display: 'flex',
  fontSize: '28px',
  backgroundColor: COLORS.LIGHT_PRIMARY,
  border: `2px solid ${COLORS.DARK_PRIMARY}`,
  position: 'absolute',
  borderRadius: '50%',
  width: `${SIZES.SATELLITE}px`,
  height: `${SIZES.SATELLITE}px`,
});

module.exports = SIZES => ({
  COLORS,
  root: {
    backgroundImage: 'radial-gradient(circle, #ccc, #444)',
  },
  viewBox: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: `${SIZES.VIEW_BOX}px`,
    height: `${SIZES.VIEW_BOX}px`,
  },
  counter: {
    display: 'flex',
    position: 'absolute',
    backgroundColor: COLORS.LIGHT_PRIMARY,
    border: `4px solid ${COLORS.DARK_PRIMARY}`,
    borderRadius: '50%',
    overflow: 'hidden',
    width: `${SIZES.COUNTER}px`,
    height: `${SIZES.COUNTER}px`,
  },
  counterContent: {
    fontSize: '36px',
    color: COLORS.PRIMARY_TEXT,
  },
  id: {
    ...satellite(SIZES),
    color: COLORS.PRIMARY_TEXT,
  },
  button: {
    ...satellite(SIZES),
    backgroundColor: COLORS.DARK_PRIMARY,
    cursor: 'pointer',
    outline: 'none',
  },
  crown: {
    position: 'absolute',
    backgroundColor: COLORS.LIGHT_PRIMARY,
    width: `${SIZES.CROWN}px`,
    height: `${SIZES.CROWN}px`,
    border: `2px solid ${COLORS.DARK_PRIMARY}`,
    borderRadius: '50%',
  },
  message: {
    position: 'absolute',
    zIndex: -2,
    opacity: 1,
    backgroundColor: COLORS.DARK_PRIMARY,
    width: `${SIZES.MESSAGE}px`,
    height: `${SIZES.MESSAGE}px`,
    borderRadius: '50%',
  },
  buttonContent: {
    fontWeight: 'bold',
    color: COLORS.ACCENT,
  },
  // used in runtime as base
  defaultContent: {
    fontWeight: 'bold',
    margin: 'auto',
    userSelect: 'none',
  },
});
