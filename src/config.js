/* globals window */
const Rx = require('rxjs');
const qs = require('querystring');

const ROOT_ID = 'app';

const DEFAULT_CONFIG = {
  size: 3,
  emitUpdatesMinMs: 1000,
  emitUpdatesMaxMs: 5000,
  deliverUpdateMinMs: 500,
  deliverUpdateMaxMs: 1000,
};

const readQueryString = () => {
  const cfg = qs.parse(window.location.search.substring(1));
  Object.keys(cfg)
    .forEach((k) => {
      if (typeof DEFAULT_CONFIG[k] === 'number') {
        cfg[k] = parseInt(cfg[k], 10) || DEFAULT_CONFIG[k];
      }
    });
  return cfg;
};

const checkConfig = (config) => {
  const errors = [];
  if (config.size < 1 || config.size > 10) errors.push('INCORRECT_SIZE expects: {1, ..., 10}');
  if (config.emitUpdatesMinMs >= config.emitUpdatesMaxMs) errors.push('INCORRECT_EMIT_UPDATES');
  if (config.deliverUpdateMinMs >= config.deliverUpdateMaxMs) errors.push('INCORRECT_DELIVER_UPDATES');
  return errors.length === 0 ? Rx.Observable.of(config) : Rx.Observable.throw({ errors, config });
};

module.exports = {
  ROOT_ID,
  handleConfig: () => Rx.Observable.of(Object.assign({}, DEFAULT_CONFIG, readQueryString()))
    .mergeMap(checkConfig)
    .catch(error => Rx.Observable.of(DEFAULT_CONFIG)
      .do(() => console.warn('INCORRECT CONFIG - FALLBACK TO DEFAULT CONFIG', error)))
    .do(cfg => window.history.replaceState(null, null, `?${qs.stringify(cfg)}`)),
};
